package main

import (
	"os/exec"
	"fmt"
	"log"
)

func main() {
	cmd := exec.Command("pgrep", "ssh")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	fmt.Printf("combined out:\n%s\n", string(out))
}